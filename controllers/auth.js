const { matchedData } = require('express-validator');
const { encrypt, compare } = require('../utils/handlePassword');
const { usersModel } = require("../models");
const { tokensign } = require("../utils/handleJwt");
const { handleHttpError } = require('../utils/handleError');

/**
 * Controlador para el registro de usuario en la plataforma
 * @param {*} req 
 * @param {*} res 
 */
const registerController = async (req, res) => {
    try {
        req = matchedData(req);
        const passwordHash = await encrypt(req.password)
        const body = { ...req, password: passwordHash }
        const dataUser = await usersModel.create(body);
        dataUser.set('password', undefined, { strict: false });

        const data = {
            token: await tokensign(dataUser),
            user: dataUser
        }
        res.send({ data });
    } catch (e) {
        console.log(e)
        handleHttpError(res, 'Error Register User', 403)
    }
}
/**
 * Controlador para el logueo del usuario
 * @param {*} req 
 * @param {*} res 
 */
const loginController = async (req, res) => {
    try {
        req = matchedData(req);
        const user = await usersModel.findOne({ email: req.email });
        console.log({ user })
        if (!user) {
            handleHttpError(res, 'Login not exists', 404)
            return
        }

        const hashPassword = user.get('password');

        const check = await compare(req.password, hashPassword);

        if (!check) {
            handleHttpError(res, 'Password invalid!', 401)
            return
        }
        user.set('password', undefined, { strict: false })
        const data = {
            'token': await tokensign(user),
            user
        }
        res.send({ data })
    } catch (e) {
        console.log(e)
        handleHttpError(res, 'Error Login User', 403)
    }
}


module.exports = { loginController, registerController }