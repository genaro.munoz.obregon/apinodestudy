const { matchedData } = require('express-validator');
const { tracksModel } = require('../models');
const { handleHttpError } = require('../utils/handleError');

/**
 * Obtener lista de la base de datos
 * @param {*} req 
 * @param {*} res 
 */

const getItems = async (req, res) => {
    try {
        //Saber que usuario esta conectado y hace peticion
        const user = req.user;

        const data = await tracksModel.findAllData();
        res.send({ data })
    } catch (e) {
        handleHttpError(res, 'Error getitems-tracks', 403)
    }

}
/**
 * Obtener un detalle de la base de datos
 * @param {*} req 
 * @param {*} res 
 */
const getItem = async (req, res) => {
    try {
        req = matchedData(req);
        const id = req.id;
        const data = await tracksModel.findOneData(id);
        res.send({ data })
    } catch (e) {
        console.log(e)
        handleHttpError(res, 'Error getitem-tracks', 403)
    }
}
/**
 * Crear registro en la base de datos
 * @param {*} req 
 * @param {*} res 
 */
const createItem = async (req, res) => {
    try {
        //Linea para usar función de express que ayuda a limpiar lo que se captura ejemplo que lo que llega viene con campos adicionales basura, solo se encarga de dejar los campos necesariso del modelo
        const body = matchedData(req)
        const data = await tracksModel.create(body)
        res.send({ data })
    } catch (e) {
        handleHttpError(res, 'Error createItem-tracks', 403)
    }

}
/**
 * Actualizar registro en la base de datos
 * @param {*} req 
 * @param {*} res 
 */
const updateItem = async (req, res) => {
    try {
        //De un objeto extraemos dos objetos, uno que soloe xtrae el id y el otro con los datos restantes.
        const { id, ...body } = matchedData(req)
        const { filter } = id
        const data = await tracksModel.findOneAndUpdate(
            filter, body, { new: false }
        )
        res.send({ data })
    } catch (e) {
        handleHttpError(res, 'Error updateItem-tracks', 403)
    }
}
/**
 * Borrar registro en la base de dato
 * @param {*} req 
 * @param {*} res 
 */
const deleteItem = async (req, res) => {
    try {
        req = matchedData(req);
        const { id } = req;
        const data = await tracksModel.delete({ _id: id });
        res.send({ data })
    } catch (e) {
        handleHttpError(res, 'Error deleteitem-tracks', 403)
    }
}


module.exports = { getItems, getItem, createItem, updateItem, deleteItem }