const jwt = require("jsonwebtoken");
const JWT_SECRET = process.env.JWT_SECRET;
const getProperties = require("../utils/handlePropertiesEngine")
const propertiesKey = getProperties();
/**
 * Debe pasar el objeto de usuario
 * @param {*} user 
 */
const tokensign = async (user) => {
    const sign = jwt.sign({
        [propertiesKey.id]: user[propertiesKey.id],
        role: user.role
    }, JWT_SECRET, {
        expiresIn: "2h",
    });

    return sign
}

/**
 * Debes pasar el token de session el JWT
 * @param {*} tokenJWT 
 * @returns 
 */

const verifytoken = async (tokenJWT) => {
    try {
        return jwt.verify(tokenJWT, JWT_SECRET)
    } catch (e) {
        return null;
    }

}

module.exports = { tokensign, verifytoken }