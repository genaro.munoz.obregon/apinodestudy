const customHeader = (req, res, next) => {
    try {
        const apiKey = req.headers.api_key;
        if (apiKey === 'GMO') {
            next
        } else {
            res.status(403)
            res.send({ error: "API KEY No es correcto" })
        }

    } catch (e) {
        res.status(403)
        res.send({ error: "Algo ocurrio en el custom header" })
    }
}