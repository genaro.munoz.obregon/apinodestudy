const { handleHttpError } = require('../utils/handleError');
/**
 * Array con los roles permitidos
 * @param {*} rol 
 * @returns 
 */
const checkRol = (roles) => (req, res, next) => {
    try {
        const { user } = req;
        const rolesByUser = user.role;
        /**
         * El some espera que la otra instrucción verifique si existe el de la derecha al lado
         */
        const checkValueRol = roles.some((rolSingle) => rolesByUser.includes(rolSingle))
        if (!checkValueRol) {
            handleHttpError(res, 'USER NOT PERMISSIONS', 403)
            return
        }
        next()

    } catch (e) {
        handleHttpError(res, 'ERROR_PERMISSIONS', 403)
    }

}

module.exports = checkRol