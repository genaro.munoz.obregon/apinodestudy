const { handleHttpError } = require('../utils/handleError');
const { verifytoken } = require("../utils/handleJwt");
const { usersModel } = require("../models")
const getProperties = require("../utils/handlePropertiesEngine")
const propertiesKey = getProperties();

const authMiddleware = async (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            handleHttpError(res, 'You need a session', 403)
            return
        }

        const token = req.headers.authorization.split(' ').pop();
        const dataToken = await verifytoken(token);

        if (!dataToken) {
            handleHttpError(res, 'Not Payload Data', 401)
            return
        }

        const query = {
            [propertiesKey.id]: dataToken[propertiesKey.id]
        }

        const user = await usersModel.findOne(query)
        req.user = user;

        next();
    } catch (e) {
        console.log(e)
        handleHttpError(res, 'Error Auth Middleware', 403)
    }
}
module.exports = authMiddleware;