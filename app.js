require("dotenv").config() //Selección de modulo de dotenv para manejo de variables de entorno
const express = require("express")
const cors = require("cors")
const ENGINE_DB = process.env.ENGINE_DB;
//Invocar conexión de mongo
const dbConnectNoSql = require("./config/mongo")
const { dbConnectMySql } = require("./config/mysql")

const app = express()

app.use(cors())
app.use(express.json())
//con eso se configura que la aplicación todo los recursos estaticos se tomen de la ruta storage
app.use(express.static("storage"))
const port = process.env.PORT || 3000


/*
Aca invocamos las rutas
*/
app.use("/api", require("./routes"))

app.listen(port, () => {
    console.log(`http:://localhost:${port}`)
});

//Llamado de conexión
(ENGINE_DB === 'nosql') ? dbConnectNoSql() : dbConnectMySql();
