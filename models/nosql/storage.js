const mongoose = require("mongoose")
const mongooseDelete = require("mongoose-delete")
const StorageScheme = new mongoose.Schema(
    //Estructura del object
    {
        url: {
            type: String
        },
        filename: {
            type: String
        }
    },
    //Este es porque nos permite crear otros campos necesarios ejemplo marca de tiempo
    {
        timestamps: true,
        versionKey: false,
    });
StorageScheme.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true })
module.exports = mongoose.model("storages", StorageScheme)