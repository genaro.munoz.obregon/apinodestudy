const mongoose = require("mongoose")
const mongooseDelete = require('mongoose-delete')
const TrackScheme = new mongoose.Schema(
    //Estructura del object
    {
        name: {
            type: String
        },
        album: {
            type: String
        },
        cover: {
            type: String,
            validate: {
                validator: (req) => {
                    return true;
                },
                message: "ERROR_URL",
            }
        },
        artist: {
            name: {
                type: String,
            },
            nickname: {
                type: String,
            },
            nationality: {
                type: String,
            }
        },
        duration: {
            start: {
                type: Number,
            },
            end: {
                type: Number,
            }
        },
        mediaId: {
            type: mongoose.Types.ObjectId,
        }
    },
    //Este es porque nos permite crear otros campos necesarios ejemplo marca de tiempo
    {
        timestamps: true,
        versionKey: false,
    });

/**
 * Implementar metodo propio con relación a storage
 */

TrackScheme.statics.findAllData = function (name) {
    const joinData = this.aggregate([
        {
            $lookup: {
                from: "storages",
                localField: "mediaId",
                foreignField: "_id",
                as: "audio"
            }
        },
        { //Hace que el resultado del join se vuelva un objeto
            $unwind: "$audio"
        }
    ])
    return joinData
};

TrackScheme.statics.findOneData = function (id) {
    const joinData = this.aggregate([
        {
            $match: {
                _id: new mongoose.Types.ObjectId(id)
            },
        },
        {
            $lookup: {
                from: "storages",
                localField: "mediaId",
                foreignField: "_id",
                as: "audio"
            }
        },
        { //Hace que el resultado del join se vuelva un objeto
            $unwind: "$audio"
        },

    ])
    return joinData
};

TrackScheme.plugin(mongooseDelete, { overrideMethods: "all", deletedAt: true })
module.exports = mongoose.model("tracks", TrackScheme)