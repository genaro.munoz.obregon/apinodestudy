const mongoose = require("mongoose")
const mongooseDelete = require("mongoose-delete")
const UserScheme = new mongoose.Schema(
    //Estructura del object
    {
        name: {
            type: String
        },
        age: {
            type: Number
        },
        email: {
            type: String,
            unique: true
        },
        password: {
            type: String,
            select: false,

        },
        role: {
            type: ["user", "admin"],
            default: "user",
        }
    },
    //Este es porque nos permite crear otros campos necesarios ejemplo marca de tiempo
    {
        timestamps: true,
        versionKey: false,
    });
UserScheme.plugin(mongooseDelete, { overrideMethods: true, deletedAt: true })
module.exports = mongoose.model("users", UserScheme)