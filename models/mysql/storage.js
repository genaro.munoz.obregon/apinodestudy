const { sequelize } = require("../../config/mysql")
const { DataTypes } = require("sequelize")

const Storage = sequelize.define(
    "storages",
    //Estructura del object
    {
        url: {
            type: DataTypes.STRING
        },
        filename: {
            type: DataTypes.STRING
        }
    },
    //Este es porque nos permite crear otros campos necesarios ejemplo marca de tiempo
    {
        timestamps: true,
    });

module.exports = Storage