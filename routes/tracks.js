const express = require("express");
const authMiddleware = require("../middleware/session");
const checkRol = require("../middleware/rol");
const { getItems, getItem, createItem, updateItem, deleteItem } = require("../controllers/tracks");
const { validatorCreateItem, validatorGetItem } = require("../validators/tracks");


const router = express.Router()

/**
 * Lista todos los items
 */
router.get("/", authMiddleware, checkRol(["admin", "user"]), getItems);
/**
 * Obtiene los registros de un item
 * Parametro :id
 */
router.get("/:id", authMiddleware, checkRol(["admin", "user"]), validatorGetItem, getItem);
/**
 * Crear un registro
 */
router.post("/", authMiddleware, checkRol(["admin", "user"]), validatorCreateItem, createItem);
/**
 * Actualizar un registro
 */
router.put("/:id", authMiddleware, checkRol(["admin"]), validatorGetItem, validatorCreateItem, updateItem);
/**
 * Borrar un registro
 */
router.delete("/:id", authMiddleware, checkRol(["admin"]), validatorGetItem, deleteItem);

module.exports = router;