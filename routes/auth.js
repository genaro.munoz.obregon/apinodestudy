const express = require("express");
const router = express.Router()
const { validatorLogin, validatorRegister } = require("../validators/auth");
const { loginController, registerController } = require("../controllers/auth")
/**
 * Registro de usuario
 */
router.post("/register", validatorRegister, registerController);
/**
 * Login
 */
router.post("/login", validatorLogin, loginController);


module.exports = router;