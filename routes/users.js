const express = require("express")
const router = express.Router()
const authMiddleware = require("../middleware/session");

router.get("/", (req, res) => {
    const data = ["hola", "mundo"]
    res.send({ data })
})
module.exports = router;