const express = require("express")
const router = express.Router()
const authMiddleware = require("../middleware/session");
const checkRol = require("../middleware/rol");
const { createItem, getItem, getItems, deleteItem } = require("../controllers/storage");
const uploadMiddleware = require("../utils/handleStorage")
const { validatorGetItem } = require("../validators/storage");


/**
 * Lista todos los items
 */
router.get("/", authMiddleware, checkRol(["admin", "user"]), getItems);
/**
 * Obtiene los registros de un item
 * Parametro :id
 */
router.get("/:id", authMiddleware, checkRol(["admin", "user"]), validatorGetItem, getItem);
/**
 * Crear un registro
 */
router.post("/", authMiddleware, checkRol(["admin", "user"]), uploadMiddleware.single("myfile"), createItem);

/**
 * Borrar un registro
 */
router.delete("/:id", authMiddleware, checkRol(["admin"]), validatorGetItem, deleteItem);

module.exports = router;