const mongoose = require("mongoose");
//Creación de cadna de conexión vacia
const dbConnect = () => {
    const DB_URI = process.env.DB_URI;
    mongoose.connect(DB_URI, {
        useNewURLParser: true,
        useUnifiedTopology: true,
    }).then(() => console.log('Conexión exitosa'))
        .catch((err) => { console.log(err) });
};

module.exports = dbConnect;