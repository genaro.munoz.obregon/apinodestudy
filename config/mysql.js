const { Sequelize } = require("sequelize")

const database = process.env.MYSQL_DATABASE;
const username = process.env.MYSQL_USERNAME;
const password = process.env.MYSQL_PASSWORD;
const host = process.env.MYSQL_HOST;

const sequelize = new Sequelize(
    database, username, password, {
    host,
    dialect: "mysql"
}
)

const dbConnectMySql = async () => {
    try {
        await sequelize.authenticate();
        console.log("Connection has been established successfully.");
    } catch (e) {
        console.log('DB error connection', e)
    }
}

module.exports = { sequelize, dbConnectMySql }