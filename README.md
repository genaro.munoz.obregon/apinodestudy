# API Node.js y Express.js con Mongodb y Mysql

## Requerimientos de ambiente

- [ ] node js versión 16.14.0 (Es la que use para la creación del proyecto)
- [ ] npm versión 8.3.1 (Es la que use para la creación del proyecto)
- [ ] VS Code
- [ ] Crear cuenta free en [mongodb](https://www.mongodb.com)

## Clonar proyecto

Para poder clonar el proyecto deberás hacer lo siguiente.

[Git clone](https://gitlab.com/genaro.munoz.obregon/apinodestudy.git)

git clone https://gitlab.com/genaro.munoz.obregon/apinodestudy.git

## Instalar dependencias

Para poder instalar las dependencias deberás hacer lo siguiente.

```
npm install
```

## Configurar variables de entorno

Para poder configurar las variables de entorno deberás hacer lo siguiente.

```
npm install dotenv
```

Las variables debe tener en cuenta los siguientes campos

```
PORT=3000
DB_URI = URI_MONGODB
PUBLIC_URL = http://localhost:3000
JWT_SECRET = LlaveMaestra
MYSQL_DATABASE=
MYSQL_USERNAME=
MYSQL_PASSWORD=
MYSQL_HOST=
ENGINE_DB=
``

## Configurar variables de entorno
Para poder iniciar el servidor deberás hacer lo siguiente.
```

npm run dev

```

```
